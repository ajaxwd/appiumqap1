Feature: user survey data selection testing
    To test user survey
    TestNG’s @BeforeClass → Cucumber’s @Before → Cucumber Background → Cucumber Scenario → Cucumber’s @After → TestNG’s @AfterClass

  Background:
    #to test production env
    #Given user is on user survey webpage https://www.dbs.com.sg/Contact/dbs/survey/default.page
    #Given user open user survey webpage https://www.dbs.com.sg/Contact/dbs/survey/default.page

    #to test UAT env
    Given user is on user survey webpage https://www.dbsuat.com.sg/Contact/dbs/survey/default.page

@Negative
  Scenario: user submit without survey details
    When user clicks on Submit data
    Then user stays on the same page with error Please select an option

@SingleDataSelectTest
  Scenario Outline: user is able to submit survey details
    When user selects data
    |PurposeofVisit|PurposeofVisitText           |Didyoufind  |DidyoufindText|SpentEffort|ContectQuality|Improvement |ImprovementText|RateSite   |RateNavigating |RateVisual |RateLoad   |RateOverall|Email|
    |2             |                             |Yes         |              |1          |1             |1           |               |1          |1              |1          |1          |1          |     |
    And user clicks on Submit data
    Then submit successfully message "<ThankYou>" is displayed
    Examples:
    |ThankYou                    |
    |Thank you for your feedback.|

@MutipleDataSelectTest
  Scenario: user is able to submit survey details
    When user selects data
    |PurposeofVisit  |PurposeofVisitText |Didyoufind  |DidyoufindText|SpentEffort|ContectQuality|Improvement |ImprovementText|RateSite   |RateNavigating |RateVisual |RateLoad   |RateOverall|Email          |
    |2,3,5           |                   |Yes         |              |2          |2             |2           |ImprovementText|2          |2              |2          |2          |2          |aaa@gmail.com  |
    And user clicks on Submit button
    Then submit successfully message is displayed

@AllDataSelectTest
  Scenario: user is able to submit survey details
    When user selects data
    |PurposeofVisit|PurposeofVisitText            |Didyoufind  |DidyoufindText|SpentEffort|ContectQuality|Improvement |ImprovementText|RateSite   |RateNavigating |RateVisual |RateLoad   |RateOverall|Email          |
    |All           |This is for Mutiple testing   |No          |No Text       |3          |3             |3           |ImprovementText|3          |3              |3          |3          |3          |               |
    And user clicks on Submit button
    Then submit successfully message is displayed

@Value4DataSelectTest
  Scenario: user is able to submit survey details
    When user selects data
    |PurposeofVisit    |PurposeofVisitText            |Didyoufind  |DidyoufindText|SpentEffort|ContectQuality|Improvement |ImprovementText|RateSite   |RateNavigating |RateVisual |RateLoad   |RateOverall|Email          |
    |1,3,5,7           |This is for Mutiple testing   |No         |No Text       |4          |4             |4           |               |4          |4              |4          |4          |4          |bbbb@aaa.com   |
    And user clicks on Submit button
    Then submit successfully message is displayed

@Value5DataSelectTest
  Scenario: user is able to submit survey details
    When user selects data
    |PurposeofVisit    |PurposeofVisitText            |Didyoufind  |DidyoufindText|SpentEffort|ContectQuality|Improvement |ImprovementText|RateSite   |RateNavigating |RateVisual |RateLoad   |RateOverall|Email          |
    |7                 |This is for Mutiple testing   |Yes         |No Text       |5          |5             |5           |               |5          |5              |5          |5          |5          |bbbb@aaa.com   |
    And user clicks on Submit button
    Then submit successfully message is displayed

@Value6DataSelectTest
  Scenario: user is able to submit survey details
    When user selects data
    |PurposeofVisit    |PurposeofVisitText            |Didyoufind  |DidyoufindText|SpentEffort|ContectQuality|Improvement |ImprovementText|RateSite   |RateNavigating |RateVisual |RateLoad   |RateOverall|Email          |
    |5,6,7             |This is for Mutiple testing   |No          |No Text       |2          |1             |6           |Testing text   |4          |5              |3          |2          |1          |bbbb@aaa.com   |
    And user clicks on Submit button
    Then submit successfully message is displayed