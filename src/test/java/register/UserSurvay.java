package register;


import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;
import userSurvayPage.SurveyPage;

import java.util.List;
import java.util.Map;

public class UserSurvay {

    //public SurveyPage SurvayPageInst = new SurveyPage();
    private WebDriver Driver;

    private SurveyPage SurvayPageInstC;

    @Given("^user (?:is on|lanuch|open|negivate to ) user survey webpage (https.*page)$")
    public void user_is_on_registration_webpage(String url) throws Exception {
        //throw new PendingException();
        SurvayPageInstC = TestRunner.SurvayPageInst;
        TestRunner.SurvayPageInst.verifyUserPage(url);
    }


    @When("^user selects data$")
    public void user_selects_data(DataTable survayData) throws Throwable {

        List<Map<String, String>> survayDatalist = survayData.asMaps(String.class, String.class);
        for (Map<String, String> survayDataselect : survayDatalist) {
            String PurposeofVisit = survayDataselect.get("PurposeofVisit");
            String PurposeofVisitText = survayDataselect.get("PurposeofVisitText");
            String Didyoufind = survayDataselect.get("Didyoufind");
            String DidyoufindText = survayDataselect.get("DidyoufindText");
            String SpentEffort = survayDataselect.get("SpentEffort");
            String ContectQuality = survayDataselect.get("ContectQuality");
            String Improvement = survayDataselect.get("Improvement");
            String ImprovementText = survayDataselect.get("ImprovementText");
            String RateSite = survayDataselect.get("RateSite");
            String RateNavigating = survayDataselect.get("RateNavigating");
            String RateVisual = survayDataselect.get("RateVisual");
            String RateLoad = survayDataselect.get("RateLoad");
            String RateOverall = survayDataselect.get("RateOverall");
            String Email = survayDataselect.get("Email");

            SurvayPageInstC.inputdata(PurposeofVisitText, DidyoufindText, ImprovementText, Email);
            SurvayPageInstC.selectData(PurposeofVisit, Didyoufind, SpentEffort, ContectQuality, Improvement, RateSite, RateNavigating, RateVisual, RateLoad, RateOverall);
        }

    }

    @Then("^user clicks on Submit button$")
    public void submit_successfully() {

    }

    @When("^user clicks on Submit data$")
    public void user_clicks_on_Submit_data() throws Throwable {
        SurvayPageInstC.clickSubmit();


    }

    @Then("^user stays on the same page with error (.*)$")
    public void user_stays_on_the_same_page(String errorMessage) throws Throwable {
        SurvayPageInstC.verifyNoSelect(errorMessage);
        SurvayPageInstC.takeScrenshot();

    }

    @Then("^submit successfully message \"(.*)\" is displayed$")
    public void submitSuccessMessage(String Thankyou) throws Throwable {
        SurvayPageInstC.verifySubmitted(Thankyou);
        SurvayPageInstC.takeScrenshot();

    }

    @Then("^submit successfully message is displayed$")
    public void submit_successfully_message_is_displayed() throws Throwable {
        System.out.println("This is test no need to submit");
    }

}
