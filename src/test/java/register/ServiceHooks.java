package register;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import webPageDriver.WebPageDriver;

public class ServiceHooks extends WebPageDriver {
   /* @Before
    public void initializeTest() {
        setWebpagedriver();
        webpagedriver.get("https://www.dbs.com.sg/Contact//dbs/survey/default.page");
    }*/

    @After
    public void embedScreenshot(Scenario scenario) {
        if (scenario.isFailed()) {
            try {
                // Code to capture and embed images in test reports (if scenario fails)
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}