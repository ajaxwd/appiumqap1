package register;


import cucumber.api.CucumberOptions;
import cucumber.api.testng.CucumberFeatureWrapper;
import cucumber.api.testng.TestNGCucumberRunner;
import org.testng.annotations.*;
import userSurvayPage.SurveyPage;
import webPageDriver.WebPageDriver;

@CucumberOptions(
        features = "src\\test\\resources\\register\\UserSurvey.feature",
        glue = {"register"},
        tags = {"~@Ignore"},
        format = {
                "pretty",
                "html:target/cucumber-reports/cucumber-pretty",
                "json:target/cucumber-reports/CucumberTestReport.json",
                "rerun:target/cucumber-reports/rerun.txt"

        })

public class TestRunner extends WebPageDriver {
    //private TestNGCucumberRunner testNGCucumberRunner;
    public static SurveyPage SurvayPageInst;
    private TestNGCucumberRunner testNGCucumberRunner;

    @BeforeClass(alwaysRun = true)
    @Parameters({"Browser", "url"})
    public void setUpClass(String Browser, String url) {
        testNGCucumberRunner = new TestNGCucumberRunner(this.getClass());
        getDriverInstance(Browser);
        //String url = "https://www.dbs.com.sg/Contact/dbs/survey/default.page";
      //  driver.get(url);
        SurvayPageInst = new SurveyPage(driver);

    }

    //groups = "cucumber",
    @Test(description = "Runs Cucumber Feature", dataProvider = "features")
    public void feature(CucumberFeatureWrapper cucumberFeature) {
        testNGCucumberRunner.runCucumber(cucumberFeature.getCucumberFeature());
    }


    @DataProvider
    public Object[][] features() {
        return testNGCucumberRunner.provideFeatures();
    }

    @AfterClass(alwaysRun = true)
    public void tearDownClass() {
        testNGCucumberRunner.finish();
        driver.quit();
    }


}

