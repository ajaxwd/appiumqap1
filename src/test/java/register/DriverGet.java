package register;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;

import java.net.MalformedURLException;
import java.net.URL;

public class DriverGet {

    protected RemoteWebDriver driver;
    protected DesiredCapabilities caps;


    @BeforeTest
    public void getDriverInstance(String BrwoserType) throws MalformedURLException {

        if (BrwoserType.equals("Firefox")){
            caps = new DesiredCapabilities();
            caps.setBrowserName("firefox");
            caps.setVersion("55.0");
            caps.setPlatform(Platform.WINDOWS);
            driver = new RemoteWebDriver(new URL("http://localhost:4444/grid/console"),caps);

        }
    }
}
