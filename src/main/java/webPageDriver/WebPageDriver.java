package webPageDriver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;


public class WebPageDriver {

    public WebDriver driver;
    private Properties properties;

    public void getDriverInstance(String Browser) {
//        System.out.println("Testing");

        if (Browser.contentEquals("FF")) {
            //System.out.println("this is for executing driver times");
            driver = new FirefoxDriver();
            driver.manage().window().maximize();

        } else if (Browser.contentEquals("CD")) {
            System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\testDataResources\\chromedriver.exe");
            driver = new ChromeDriver();
            driver.manage().window().maximize();
        }

    }

    protected Properties setFile() throws FileNotFoundException {
        String PropertyPath = "src\\main\\configs\\elemconfiguration.properties";
        FileReader getPropperty = new FileReader(PropertyPath);
        BufferedReader bufferFileReader = new BufferedReader(getPropperty);
        try {
            properties = new Properties();
            properties.load(bufferFileReader);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return properties;
    }

//    public void setFileInput() throws FileNotFoundException {
//        String PropertyPath = "D:\\Assignment\\CucumberSelenium_Website\\CucumberSelenium_Demo_Completed\\CucumberProject\\src\\main\\configs\\elemconfiguration.properties"
//        File getPropperty = new File(PropertyPath);
//        FileInputStream FileReader = new FileInputStream(getPropperty);
//
//        try {
//            properties = new Properties();
//            properties.load(FileReader);
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//    }
}