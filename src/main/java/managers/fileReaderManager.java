package managers;

import dataDriver.configFileReader;

public class fileReaderManager {


    private static fileReaderManager currentfileReaderManager = new fileReaderManager();
    private configFileReader currentConfigFileReader;

    private fileReaderManager() {
    }

    private static fileReaderManager getInstance(){
        return currentfileReaderManager;
    }
    public configFileReader getConfigReader(){
        return (currentConfigFileReader == null) ? new configFileReader() : currentConfigFileReader;
    }
}
