package register;

import com.jayway.jsonpath.JsonPath;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


import java.io.File;
import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.Properties;

public class elemtLocation {
    private final WebDriver driver;
    private final Properties properties;

    public elemtLocation(WebDriver driver, Properties properties) {
        this.driver = driver;
        this.properties = properties;
    }


    public void AgreeTermButton() {

        String getCurrentelem = properties.getProperty("agreeButtonName");
        WebElement CurrentelemAgree = driver.findElement(By.name(getCurrentelem));
        CurrentelemAgree.click();
    }

    public void NotAgreeTermButton() {

        String getCurrentelem = properties.getProperty("notAgreeButtonName");
        WebElement CurrentelemNotAgree = driver.findElement(By.name(getCurrentelem));

        CurrentelemNotAgree.click();
    }

    public void VerifyRegistrationText() {

        try {
            String getCurrentelem = properties.getProperty("RegistrationTextXpath");
            String registertext = driver.findElement(By.xpath(getCurrentelem)).getText();
            // contains("Ranorex Forum - Registration");
            System.out.println("User agress itmes : "+ registertext);
        } catch (Exception e) {
            System.out.println("User disagree to register");
        }
        // Assert.assertEquals(actualTtext,expectedError);
    }

    public WebElement getUsernameElemDataRegisterPage(){
        String getUsername = properties.getProperty("usernameName");
        WebElement getUsernameElem = driver.findElement(By.name(getUsername));
        return getUsernameElem;
    }

    public WebElement getEmailElemDataRegisterPage(){
        String getEmail = properties.getProperty("emailName");
        WebElement getEmailElem = driver.findElement(By.name(getEmail));
        return getEmailElem;
    }

    public WebElement getConfirmedEmailElemDataRegisterPage(){
        String getEmailConfirm = properties.getProperty("emailConfirmName");
        WebElement getConfirmedEmailElem = driver.findElement(By.name(getEmailConfirm));
        return getConfirmedEmailElem;
    }

    public WebElement getPasswordElemDataRegisterPage(){
        String getPassword = properties.getProperty("passwordName");
        WebElement getPasswordElem = driver.findElement(By.name(getPassword));
        return getPasswordElem;
    }

    public WebElement getPasswordConfirmElemDataRegisterPage(){
        String getPasswordConfirm = properties.getProperty("passwordConfirmName");
        WebElement getPasswordConfirmElem = driver.findElement(By.name(getPasswordConfirm));
        return getPasswordConfirmElem;
    }
    public WebElement getOurCompanyElemDataRegisterPage(){
        String getOurCompany = properties.getProperty("ourCompanyName");
        WebElement getOurCompanyElem = driver.findElement(By.name(getOurCompany));
        return getOurCompanyElem;
    }
    public WebElement getCompanyElemElemDataRegisterPage(){
        String getCompany = properties.getProperty("companyName");
        WebElement getCompanyElem = driver.findElement(By.name(getCompany));
        return getCompanyElem;
    }


    public WebElement getSubmitButtonRegisterPage(){
        String getSubmitButton = properties.getProperty("submitButtonName");
        WebElement getSubmitButtonRegister = driver.findElement(By.name(getSubmitButton));
        return getSubmitButtonRegister;
    }

    public WebElement getEmailErrorMessageRegisterPage(){
        String getEmailError = properties.getProperty("failedMessageEmailXpath");
        WebElement getEmailErrorElem = driver.findElement(By.xpath(getEmailError));
        return getEmailErrorElem;
    }
    public WebElement getPassMessageRegisterPage(){
        String getPassMessage = properties.getProperty("RegisteredPsssMessageXpath");
        WebElement getPassMessageElem = driver.findElement(By.xpath(getPassMessage));
        if (getPassMessageElem !=null) return getPassMessageElem;
        else throw new NoSuchElementException();
    }


    public void enterRegisterData(String userName, String emailAdd, String confirmEmailAddr, String passwordWord, String confirmPassword, String ourCompanyName, String company) {

        WebElement getUsernameElemDataRegisterPage = getUsernameElemDataRegisterPage();
        WebElement getEmailElemDataRegisterPage = getEmailElemDataRegisterPage();
        WebElement getConfirmedEmailElemDataRegisterPage = getConfirmedEmailElemDataRegisterPage();
        WebElement getPasswordElemDataRegisterPage = getPasswordElemDataRegisterPage();
        WebElement getPasswordConfirmElemDataRegisterPage = getPasswordConfirmElemDataRegisterPage();
        WebElement getOurCompanyElemDataRegisterPage = getOurCompanyElemDataRegisterPage();
        WebElement getCompanyElemElemDataRegisterPage = getCompanyElemElemDataRegisterPage();

        getUsernameElemDataRegisterPage.sendKeys(userName);
        getEmailElemDataRegisterPage.sendKeys(emailAdd);
        getConfirmedEmailElemDataRegisterPage.sendKeys(confirmEmailAddr);
        getPasswordElemDataRegisterPage.sendKeys(passwordWord);;
        getPasswordConfirmElemDataRegisterPage.sendKeys(confirmPassword);
        getOurCompanyElemDataRegisterPage.sendKeys(ourCompanyName);
        getCompanyElemElemDataRegisterPage.sendKeys(company);
   }

   public void SubmitRegisterData(){
      WebElement getSubmitButtonRegisterPage = getSubmitButtonRegisterPage();
       getSubmitButtonRegisterPage.click();
   }

   public String GetUserNameFromJason() throws IOException {
        File getjsonFilePath = new File(properties.getProperty("jasonFilePath"));
        String UserName = JsonPath.read(getjsonFilePath, "$." + "UserName");
        //System.out.println (UserName);
        return UserName;
   }
   public String GetEmailFromJason() throws IOException {
        File getjsonFilePath = new File(properties.getProperty("jasonFilePath"));
        String EmailAdd = JsonPath.read(getjsonFilePath, "$." + "EmailAdd");
        //System.out.println (EmailAdd);
        return EmailAdd;
   }
   public String GetConfirmEmailAddrFromJason() throws IOException {
        File getjsonFilePath = new File(properties.getProperty("jasonFilePath"));
        String ConfirmEmailAddr = JsonPath.read(getjsonFilePath, "$." + "ConfirmEmailAddr");
        //System.out.println (ConfirmEmailAddr);
        return ConfirmEmailAddr;
   }

   public String GetPasswordWordFromJason() throws IOException {
        File getjsonFilePath = new File(properties.getProperty("jasonFilePath"));
        String PasswordWord = JsonPath.read(getjsonFilePath, "$." + "PasswordWord");
        //System.out.println (PasswordWord);
        return PasswordWord;
    }

   public String GetConfirmPasswordFromJason() throws IOException {
        File getjsonFilePath = new File(properties.getProperty("jasonFilePath"));

        String ConfirmPassword = JsonPath.read(getjsonFilePath, "$." + "ConfirmPassword");
        String OurCompanyName = JsonPath.read(getjsonFilePath, "$." + "OurCompanyName");
        //String TimeZone = JsonPath.read(getjsonFilePath, "$." + "TimeZone");
        String Company = JsonPath.read(getjsonFilePath, "$." + "Company");
        //System.out.println (ConfirmPassword);
        return ConfirmPassword;
    }
   public String GetOurCompanyNameFromJason() throws IOException {
        File getjsonFilePath = new File(properties.getProperty("jasonFilePath"));
        String OurCompanyName = JsonPath.read(getjsonFilePath, "$." + "OurCompanyName");
        //String TimeZone = JsonPath.read(getjsonFilePath, "$." + "TimeZone");
        String Company = JsonPath.read(getjsonFilePath, "$." + "Company");
        //System.out.println (OurCompanyName);
        return OurCompanyName;
    }
   public String GetCompanyFromJason() throws IOException {
        File getjsonFilePath = new File(properties.getProperty("jasonFilePath"));
        String Company = JsonPath.read(getjsonFilePath, "$." + "Company");
        //System.out.println (Company);
        return Company;
    }

   public void verifiedEmailMessage(String ErrorMessage) {
        String EmailErrorMessage = getEmailErrorMessageRegisterPage().getText();
       // System.out.println("here is to print email error"+EmailErrorMessage);
       // Assert.assertEquals(ErrorMessage,EmailErrorMessage);
   }

   public void verifiedPassMessage(String PassTitle) {
        String PassedMessage = getPassMessageRegisterPage().getText();
       // String PassedMessage ="Your account has been created.However, this board requires account activation, an activation key has been sent to the e-mail address you provided. Please check your e-mail for further information.";
        String FinalPassMessage = PassedMessage.substring(0,30);
        System.out.println("here is to print successful message: "+FinalPassMessage);
        if (PassTitle.contentEquals(FinalPassMessage)){
            System.out.println("expected");

       }else
        {
            System.out.println("not expected");
        }

        //Assert.assertEquals(PassTitle,FinalPassMessage);
   }
}
