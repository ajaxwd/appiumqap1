package register;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import java.io.IOException;

public class registerPageLocator {


    public registerPageLocator(WebDriver driver) throws IOException {
        PageFactory.initElements(driver, this);


    }

    @FindBy(how = How.NAME, using = "usernameName")
    private WebElement usernameName;
    @FindBy(how = How.NAME, using = "emailName")
    private WebElement emailName;

    @FindBy(how = How.NAME, using = "emailConfirmName")
    private WebElement emailConfirmName;

    @FindBy(how = How.NAME, using = "passwordName")
    private WebElement passwordName;

    @FindBy(how = How.NAME, using = "passwordConfirmName")
    private WebElement passwordConfirmName;

    @FindBy(how = How.NAME, using = "ourCompanyName")
    private WebElement ourCompanyName;

    @FindBy(how = How.NAME, using = "companyName")
    private WebElement companyName;

    @FindBy(how = How.NAME, using = "submitButton")
    private WebElement submitButtonName;


    public void sentDataRegister(String UserName, String EmailAdd, String ConfirmEmailAddr, String PasswordWord, String ConfirmPassword, String OurCompanyName, String Company) {
        usernameName.sendKeys(UserName);
        emailName.sendKeys(EmailAdd);
        emailConfirmName.sendKeys(ConfirmEmailAddr);
        passwordName.sendKeys(PasswordWord);
        passwordConfirmName.sendKeys(ConfirmPassword);
        ourCompanyName.sendKeys(OurCompanyName);
        companyName.sendKeys(Company);
    }

    public void setSubmitButtonName() {
        System.out.println("heriesi to print weblemnt" + submitButtonName);
        submitButtonName.click();
    }

/*    public String getLocatorFile() throws IOException {

        String propertyFilePath = "D:\\Cucumber\\CucumberProject\\src\\main\\configs\\elemconfiguration.properties";
        //   File file = new File (propertyFilePath);

        BufferedReader reader = new BufferedReader(new FileReader(propertyFilePath));
        Properties properties = new Properties();
        properties.load(reader);
        String getCurrentelem = properties.getProperty("usernameName");
        return getCurrentelem;
    }*/
}



