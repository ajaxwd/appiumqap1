package userSurvayPage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import webPageDriver.WebPageDriver;

import java.io.FileNotFoundException;

class GetElement extends WebPageDriver {

    private WebDriver webpagedriver;
    GetElement(WebDriver webpagedriver) {
        this.webpagedriver = webpagedriver;
    }

    WebElement getwebelemtbyxpath(String ElemXpath) {
        return webpagedriver.findElement(By.xpath(ElemXpath));
    }


    String Purposeofvisit(String PurposeofvisitId) throws FileNotFoundException {
        String Purposeofvisit = setFile().getProperty("Purposeofvisit");
        return Purposeofvisit + "[" + PurposeofvisitId + "]";

    }

    String PurposeofvisitText() throws FileNotFoundException {
        return setFile().getProperty("PurposeofvisitText");
    }

    String Didyoufind(String DidyouFindValue) throws FileNotFoundException {
        String didYouFind = null;
        if (DidyouFindValue.contentEquals("Yes")) {
            didYouFind = setFile().getProperty("DidyoufindYes");
        } else if (DidyouFindValue.contentEquals("No")) {
            didYouFind = setFile().getProperty("DidyoufindNo");
        }
        return didYouFind;
    }

    String DidyoufindText() throws FileNotFoundException {
        return setFile().getProperty("DidyoufindText");
    }

    String Spenteffort(String SpenteffortId) throws FileNotFoundException {
        String Spenteffort = setFile().getProperty("Spenteffort");
        return Spenteffort + "[@value='" + SpenteffortId + "']";

    }

    String Contectquality(String ContectQualityId) throws FileNotFoundException {
        String Contectquality = setFile().getProperty("Contectquality");
        return Contectquality + "[@value='" + ContectQualityId + "']";
    }

    String Improvement(int ImprovementId) throws FileNotFoundException {
        String Improvement = setFile().getProperty("Improvement");
        return Improvement + "span[" + ImprovementId + "]/input";
    }

    String ImprovementText() throws FileNotFoundException {
        return setFile().getProperty("ImprovementText");
    }

    String Ratethissite(String RateSiteID) throws FileNotFoundException {
        String Ratethissite = setFile().getProperty("Ratethissite");
        return Ratethissite + "[@value='" + RateSiteID + "']";
    }

    String Ratenavigating(String RatenavigatingId) throws FileNotFoundException {
        String Ratenavigating = setFile().getProperty("Ratenavigating");
        return Ratenavigating + "[@value='" + RatenavigatingId + "']";
    }

    String Ratevisual(String RatevisualId) throws FileNotFoundException {
        String Ratevisual = setFile().getProperty("Ratevisual");
        return Ratevisual + "[@value='" + RatevisualId + "']";
    }

    String RateLoad(String RateLoadId) throws FileNotFoundException {
        String RateLoad = setFile().getProperty("RateLoad");
        return RateLoad + "[@value='" + RateLoadId + "']";
    }

    String RateOverall(String RateOverallID) throws FileNotFoundException {
        String RateOverall = setFile().getProperty("RateOverall");
        return RateOverall + "[@value='" + RateOverallID + "']";
    }

    String Email() throws FileNotFoundException {
        return setFile().getProperty("Email");
    }

    String SubmitButton() throws FileNotFoundException {
        return setFile().getProperty("submitButton");
    }

    String errorMessageSelect() throws FileNotFoundException {
        return setFile().getProperty("NoselectError");
    }

    String submitedSuccessMessage() throws FileNotFoundException {
        return setFile().getProperty("Thankyou");
    }

}