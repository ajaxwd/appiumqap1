package userSurvayPage;

import org.codehaus.plexus.util.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import webPageDriver.WebPageDriver;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.testng.Assert.assertEquals;

public class SurveyPage extends WebPageDriver {
    private GetElement selectData;

    public SurveyPage(WebDriver driver) {
        this.driver = driver;
    }

    public void verifyUserPage(String url) throws Exception {
        driver.get(url);
        selectData = new GetElement(driver);

        try {
            Assert.assertEquals("DBS Survey | DBS Singapore", driver.getTitle());
            System.out.println("User is on survey page");
        } catch (Exception exp) {
            System.out.println("User is on survey page");

            throw exp;
        }
      /*  if (CurrentPageTile.contentEquals("DBS Survey | DBS Singapore")) {
            System.out.println("User is on survey page");
        } else {
            System.out.println("User is not on the page");
        }*/
    }

    public void clickSubmit() throws FileNotFoundException {
        selectData.getwebelemtbyxpath(selectData.SubmitButton()).click();
    }

    public void verifyNoSelect(String errorMessage) throws FileNotFoundException {
        // System.out.println("herekdkasdjfaslf" + selectData.getwebelemtbyxpath(selectData.errorMessageSelect()).getText());
        if (selectData.getwebelemtbyxpath(selectData.errorMessageSelect()).getText().contentEquals(errorMessage)) {
            System.out.println("Expected:" + errorMessage);
        } else {
            System.out.println("Unexpected - Did not see " + errorMessage);
        }
    }

    public void verifySubmitted(String thankyou) throws Exception {
        //  System.out.println("herekdkasdjfaslf" + selectData.getwebelemtbyxpath(selectData.submitedSuccessMessage()).getText());

        try {
            assertEquals(selectData.getwebelemtbyxpath(selectData.submitedSuccessMessage()).getText(),thankyou);
        } catch (Exception exp) {
            throw exp;
        }
/*
        if (selectData.getwebelemtbyxpath(selectData.submitedSuccessMessage()).getText().contentEquals(thankyou)){
            System.out.println(thankyou);
        } else
        {
            System.out.println("Submitted data failed");

        }
*/
    }


    private void Purposeofvisit(String PurposeofvisitId) throws FileNotFoundException {
        if (PurposeofvisitId.contentEquals("1")) {
            selectData.getwebelemtbyxpath(selectData.Purposeofvisit(PurposeofvisitId)).click();
        } else if (PurposeofvisitId.contentEquals("2")) {
            selectData.getwebelemtbyxpath(selectData.Purposeofvisit(PurposeofvisitId)).click();
        } else if (PurposeofvisitId.contentEquals("3")) {
            selectData.getwebelemtbyxpath(selectData.Purposeofvisit(PurposeofvisitId)).click();
        } else if (PurposeofvisitId.contentEquals("4")) {
            selectData.getwebelemtbyxpath(selectData.Purposeofvisit(PurposeofvisitId)).click();
        } else if (PurposeofvisitId.contentEquals("5")) {
            selectData.getwebelemtbyxpath(selectData.Purposeofvisit(PurposeofvisitId)).click();
        } else if (PurposeofvisitId.contentEquals("6")) {
            selectData.getwebelemtbyxpath(selectData.Purposeofvisit(PurposeofvisitId)).click();
        } else if (PurposeofvisitId.contentEquals("7")) {
            selectData.getwebelemtbyxpath(selectData.Purposeofvisit(PurposeofvisitId)).click();
        }
    }

    private void purposeText(String purposeText) throws FileNotFoundException {

        selectData.getwebelemtbyxpath(selectData.PurposeofvisitText()).sendKeys(purposeText);
    }

    private void Didyoufind(String Didyoufind) throws FileNotFoundException {

        selectData.getwebelemtbyxpath(selectData.Didyoufind(Didyoufind)).click();
    }

    private void DidyoufindText(String DidyoufindText) throws FileNotFoundException {

        selectData.getwebelemtbyxpath(selectData.DidyoufindText()).sendKeys(DidyoufindText);
    }

    private void Spenteffort(String SpenteffortId) throws FileNotFoundException {
        if (SpenteffortId.contentEquals("1")) {
            selectData.getwebelemtbyxpath(selectData.Spenteffort(SpenteffortId)).click();
        } else if (SpenteffortId.contentEquals("2")) {
            selectData.getwebelemtbyxpath(selectData.Spenteffort(SpenteffortId)).click();
        } else if (SpenteffortId.contentEquals("3")) {
            selectData.getwebelemtbyxpath(selectData.Spenteffort(SpenteffortId)).click();
        } else if (SpenteffortId.contentEquals("4")) {
            selectData.getwebelemtbyxpath(selectData.Spenteffort(SpenteffortId)).click();
        } else if (SpenteffortId.contentEquals("5")) {
            selectData.getwebelemtbyxpath(selectData.Spenteffort(SpenteffortId)).click();
        }
    }

    private void Contectquality(String ContectqualityId) throws FileNotFoundException {

        if (ContectqualityId.contentEquals("1")) {
            selectData.getwebelemtbyxpath(selectData.Contectquality(ContectqualityId)).click();
        } else if (ContectqualityId.contentEquals("2")) {
            selectData.getwebelemtbyxpath(selectData.Contectquality(ContectqualityId)).click();
        } else if (ContectqualityId.contentEquals("3")) {
            selectData.getwebelemtbyxpath(selectData.Contectquality(ContectqualityId)).click();
        } else if (ContectqualityId.contentEquals("4")) {
            selectData.getwebelemtbyxpath(selectData.Contectquality(ContectqualityId)).click();
        } else if (ContectqualityId.contentEquals("5")) {
            selectData.getwebelemtbyxpath(selectData.Contectquality(ContectqualityId)).click();
        }
    }

    private void Improvement(String ImprovementId) throws FileNotFoundException {

        if (Integer.parseInt(ImprovementId) == 1) {
            int SelectImprovementId = Integer.parseInt(ImprovementId) + 1;
            selectData.getwebelemtbyxpath(selectData.Improvement(SelectImprovementId)).click();
        } else if (Integer.parseInt(ImprovementId) == 2) {
            int SelectImprovementId = Integer.parseInt(ImprovementId) + 2;
            selectData.getwebelemtbyxpath(selectData.Improvement(SelectImprovementId)).click();
        } else if (Integer.parseInt(ImprovementId) == 3) {
            int SelectImprovementId = Integer.parseInt(ImprovementId) + 3;
            selectData.getwebelemtbyxpath(selectData.Improvement(SelectImprovementId)).click();

        } else if (Integer.parseInt(ImprovementId) == 4) {
            int SelectImprovementId = Integer.parseInt(ImprovementId) + 4;
            selectData.getwebelemtbyxpath(selectData.Improvement(SelectImprovementId)).click();
        } else if (Integer.parseInt(ImprovementId) == 5) {
            int SelectImprovementId = Integer.parseInt(ImprovementId) + 5;
            selectData.getwebelemtbyxpath(selectData.Improvement(SelectImprovementId)).click();
        } else if (Integer.parseInt(ImprovementId) == 6) {
            int SelectImprovementId = Integer.parseInt(ImprovementId) + 6;
            selectData.getwebelemtbyxpath(selectData.Improvement(SelectImprovementId)).click();
        }
    }

    private void ImprovementText(String ImprovementText) throws FileNotFoundException {
        selectData.getwebelemtbyxpath(selectData.ImprovementText()).sendKeys(ImprovementText);
    }


    private void Ratethissite(String RatethissiteId) throws FileNotFoundException {
        if (RatethissiteId.contentEquals("1")) {
            selectData.getwebelemtbyxpath(selectData.Ratethissite(RatethissiteId)).click();
        } else if (RatethissiteId.contentEquals("2")) {
            selectData.getwebelemtbyxpath(selectData.Ratethissite(RatethissiteId)).click();
        } else if (RatethissiteId.contentEquals("3")) {
            selectData.getwebelemtbyxpath(selectData.Ratethissite(RatethissiteId)).click();
        } else if (RatethissiteId.contentEquals("4")) {
            selectData.getwebelemtbyxpath(selectData.Ratethissite(RatethissiteId)).click();
        } else if (RatethissiteId.contentEquals("5")) {
            selectData.getwebelemtbyxpath(selectData.Ratethissite(RatethissiteId)).click();
        }
    }

    private void Ratenavigating(String RatenavigatingId) throws FileNotFoundException {
        if (RatenavigatingId.contentEquals("1")) {
            selectData.getwebelemtbyxpath(selectData.Ratenavigating(RatenavigatingId)).click();
        } else if (RatenavigatingId.contentEquals("2")) {
            selectData.getwebelemtbyxpath(selectData.Ratenavigating(RatenavigatingId)).click();
        } else if (RatenavigatingId.contentEquals("3")) {
            selectData.getwebelemtbyxpath(selectData.Ratenavigating(RatenavigatingId)).click();
        } else if (RatenavigatingId.contentEquals("4")) {
            selectData.getwebelemtbyxpath(selectData.Ratenavigating(RatenavigatingId)).click();
        } else if (RatenavigatingId.contentEquals("5")) {
            selectData.getwebelemtbyxpath(selectData.Ratenavigating(RatenavigatingId)).click();
        }
    }

    private void Ratevisual(String RatevisualId) throws FileNotFoundException {
        if (RatevisualId.contentEquals("1")) {
            selectData.getwebelemtbyxpath(selectData.Ratevisual(RatevisualId)).click();
        } else if (RatevisualId.contentEquals("2")) {
            selectData.getwebelemtbyxpath(selectData.Ratevisual(RatevisualId)).click();
        } else if (RatevisualId.contentEquals("3")) {
            selectData.getwebelemtbyxpath(selectData.Ratevisual(RatevisualId)).click();
        } else if (RatevisualId.contentEquals("4")) {
            selectData.getwebelemtbyxpath(selectData.Ratevisual(RatevisualId)).click();
        } else if (RatevisualId.contentEquals("5")) {
            selectData.getwebelemtbyxpath(selectData.Ratevisual(RatevisualId)).click();
        }
    }

    private void RateLoad(String RateLoadId) throws FileNotFoundException {
        if (RateLoadId.contentEquals("1")) {
            selectData.getwebelemtbyxpath(selectData.RateLoad(RateLoadId)).click();
        } else if (RateLoadId.contentEquals("2")) {
            selectData.getwebelemtbyxpath(selectData.RateLoad(RateLoadId)).click();
        } else if (RateLoadId.contentEquals("3")) {
            selectData.getwebelemtbyxpath(selectData.RateLoad(RateLoadId)).click();
        } else if (RateLoadId.contentEquals("4")) {
            selectData.getwebelemtbyxpath(selectData.RateLoad(RateLoadId)).click();
        } else if (RateLoadId.contentEquals("5")) {
            selectData.getwebelemtbyxpath(selectData.RateLoad(RateLoadId)).click();
        }
    }

    private void RateOverall(String RateOverallId) throws FileNotFoundException {
        if (RateOverallId.contentEquals("1")) {
            selectData.getwebelemtbyxpath(selectData.RateOverall(RateOverallId)).click();
        } else if (RateOverallId.contentEquals("2")) {
            selectData.getwebelemtbyxpath(selectData.RateOverall(RateOverallId)).click();
        } else if (RateOverallId.contentEquals("3")) {
            selectData.getwebelemtbyxpath(selectData.RateOverall(RateOverallId)).click();
        } else if (RateOverallId.contentEquals("4")) {
            selectData.getwebelemtbyxpath(selectData.RateOverall(RateOverallId)).click();
        } else if (RateOverallId.contentEquals("5")) {
            selectData.getwebelemtbyxpath(selectData.RateOverall(RateOverallId)).click();
        }
    }

    private void Email(String Email) throws FileNotFoundException {
        selectData.getwebelemtbyxpath(selectData.Email()).sendKeys(Email);

    }

    public void inputdata(String PurposeofVisitText, String DidyoufindText, String ImprovementText, String Email) throws FileNotFoundException {

        if (!PurposeofVisitText.contentEquals("")) {
            purposeText(PurposeofVisitText);
        }

        if (!DidyoufindText.contentEquals("")) {
            DidyoufindText(DidyoufindText);
        }
        if (!Email.contentEquals("")) {
            Email(Email);
        }
        if (!ImprovementText.contentEquals("")) {
            ImprovementText(ImprovementText);
        }
    }

    public void selectData(String PurposeofVisit, String Didyoufind, String SpentEffort, String ContectQuality, String Improvement, String RateSite, String RateNavigating, String RateVisual, String RateLoad, String RateOverall) throws FileNotFoundException {
        if (PurposeofVisit.contentEquals("All")) {
            for (int i = 1; i < 8; i++) {
                String selectedVisitID = Integer.toString(i);
                Purposeofvisit(selectedVisitID);
            }
        } else if (PurposeofVisit.contains(",")) {
            String[] MutipleVisit = PurposeofVisit.split(",");
            //System.out.println(Arrays.toString(MutipleVisit));
            //for (int i =0; i<MutipleVisit.length;i++);
            for (String MutipleVisitSelect : MutipleVisit) {
                Purposeofvisit(MutipleVisitSelect);
            }
        } else {
            Purposeofvisit(PurposeofVisit);
        }
        Didyoufind(Didyoufind);
        Spenteffort(SpentEffort);
        Contectquality(ContectQuality);
        Improvement(Improvement);
        Ratethissite(RateSite);
        Ratenavigating(RateNavigating);
        RateLoad(RateLoad);
        Ratevisual(RateVisual);
        RateOverall(RateOverall);
    }

    public void takeScrenshot() throws IOException {
        String folderName = "userSurvey";
        File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        //create dir with given folder name
        new File(folderName).mkdir();
        SimpleDateFormat fileFormat = new SimpleDateFormat("dd-MMM-yyyy_hh_mm_ssaa");
        //Setting file name
        String fileName = fileFormat.format(new Date()) + ".png";
        //coppy screenshot file into screenshot folder.
        FileUtils.copyFile(scrFile, new File(folderName + "/" + fileName));
    }
}
